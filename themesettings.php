<?php include "templates/start.php"; ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Handleiding Italstudio</title>
    <?php include "templates/head.php"; ?>

</head>
<body>

  <?php include "templates/header.php"; ?>
  <div class="d-none d-xl-block fixed-top tableofcontent">
        <div class="brickHouse position-absolute">
                    <div class="btn-slab text-center">
                      <a data-toggle="collapse" href="#Inhoud" role="button" aria-expanded="false" aria-controls="Inhoud" class="bg-blue text-white rounded-left px-5 py-3">
                        Inhoudsopgave
                      </a>
                    </div>
                    <div class="collapse width" id="Inhoud">
                        <div class="card-contentOelbert text-white rounded-left p-5 pl-2 right-extra bg-blue">
                          <h6 class="text-white">Navigeer naar een onderwerp</h6>
                          <a href="#settings" class="scrollto text-white">Blokken</a><br/>
                          <a href="#contact" class="scrollto text-white">Contact</a><br/>
                          <a href="#footer" class="scrollto text-white">Footer</a><br/>

                       </div>
                    </div>
               </div>
            </div>
          </div>
  <section class="intro">
    <div id="accordionExample">
      <i class="fas fa-info-circle mb-4 greentext fa-2x collapsed ml-2 ml-xl-5" data-toggle="collapse" href="#collapseExample2" data-target="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2" style="cursor:pointer"></i>
      <div class="carret collapse fade" id="collapseExample2" aria-labelledby="headingOne" data-parent="#accordionExample"></div>
    </div>
    <div class="collapse w-100 bg-graylight mb-5 border-top border-bottom" id="collapseExample2" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="container">
        <div class="row">
          <div class="p-4 p-xl-5">
               <h2>Werken met Gutenberg?</h2>
               <p class="lead">In deze handleiding zullen we je laten zien hoe Gutenberg werkt en wat je er allemaal mee kunt. Wil je direct naar een specifiek onderdeel van Gutenberg kijken? Gebruik dan de inhoudsopgave rechts.</p>
               <p class="lead">Pagina's aanpassen werkt op de standaard manier. Wij hebben in het CMS "Theme settings" aangemaakt daarmee kan je blokken aanpassen die op meerdere plekken in de site aanwezig zijn. <strong>Werving</strong>, <strong>Contact</strong>, <strong>Homepage</strong> en de <strong>Footer</strong>.</p>
               <p class="lead">Bij het aanpassen van content hou er aub rekening mee dat foto's klein zijn (bijvoorbeeld 5 mb is niet de bedoeling daar wordt de website traag van) Dus eerst op maat maken in bijvoorbeeld Photoshop en dan verkleinen in online website als tinypng of kraken.io</p>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-md-7 mx-auto content">
            <h1>Theme settings</h1>

            <p>Wij hebben in het CMS een knop <span class="bluetext">"Italstudio Settings"</span> aangemaakt daarmee kan je verschillende blokken op de website aanpassen. </p>
             <img src="assets/img/settings.jpg" alt="theme settings" style="padding:10px 10px 10px 0;"/> <br/>

            <h2 class="bluetext mt-lg-5" id="settings">Blokken</h2>
            <p>Hier kan je blokken aanpassen die op meerdere plekken voorkomen op de website. Slideshow, 3x imageblok en Waarom Italstudio. De werking spreekt voor zich. </p>
            
            <p>Bij het uploaden van images zorg ervoor dat ze op het goede formaat zijn gesneden en gecomprimeerd.<br/>
            Gebruik hiervoor tools als tinypng of kraken.io</p>
             <img src="assets/img/slideshow.jpg" alt="voorbeeld slideshow" style="padding:10px 10px 10px 0;" class="img-fluid"/> <br/>
             <img src="assets/img/images3x.jpg" alt="voorbeeld images 3 x" style="padding:10px 10px 10px 0;" class="img-fluid"/> <br/>
             <img src="assets/img/waarom.jpg" alt="voorbeeld waarom" style="padding:10px 10px 10px 0;" class="img-fluid"/> <br/>
             
             <p style"font-size:10px;">staat nu op max. 6</p>


            <h2 class="bluetext mt-lg-5" id="contact">Contact</h2>
            <p>Dit blok is het centrale punt voor alle contactgegevens. Wijzig je hier iets dan wordt het doorgevoerd op de gehele website.</p>
            <p>De gegevens zijn dus herbruikbaar en inzetbaar door middel van een shortcode.

            Bijvoorbeeld op de contactpagina. Hoe werkt dit? <br/>
            Bewerk de contactpagina en klik op toevoegen blok > shortcode </p>

            <p>Begin en sluit af met een bracket vervolgens begin je altijd met het woord contact gevolgd door een underscore en dan adres, email of telefoon <br/>
            [contact_telefoon]<br/>
            [contact_email]</p>

            <img src="assets/img/shortcode.jpg" alt="voorbeeld shortcode" style="padding:10px 10px 10px 0;" class="img-fluid"/> <br/>

            <h2 class="bluetext mt-lg-5" class="bluetext" id="footer">Footer</h2>
            <p>De footer bestaat uit 4 blokken.</p>

            <p>Het eerste blok is een over ons (tekstblok) met een korte introductie tekst.</p>

            <p>Het tweede blok is een het adres blok.</p>

            <p>Het derde blok is een links blok onderverdeeld in de 3 stromingen.</p>

            <p>Het vierde blok is een menublok met links naar algemene pagina's. Deze is te vinden onder Weergave > Menu > Footermenu</p>


           

        </div>
      </div>
  </section>
   
  <?php include "templates/footer.php"; ?>
  </body>
</html>