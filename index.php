<?php include "templates/start.php"; ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Wordpress handleiding | Italstudio </title>
    <?php include "templates/head.php"; ?>

</head>
<body>

  <?php include "templates/header.php"; ?>
  <div class="d-none d-xl-block fixed-top tableofcontent">
        <div class="brickHouse position-absolute">
                    <div class="btn-slab text-center">
                      <a data-toggle="collapse" href="#Inhoud" role="button" aria-expanded="false" aria-controls="Inhoud" class="bg-blue text-white rounded-left px-5 py-3">
                        Inhoudsopgave
                      </a>
                    </div>
                    <div class="collapse width" id="Inhoud">
                        <div class="card-contentOelbert text-white rounded-left p-5 pl-2 right-extra bg-blue">
                          <h6 class="text-white">Navigeer naar een onderwerp</h6>
                          <a href="#titel" class="scrollto text-white">Titel</a><br/>
                          <a href="#content" class="scrollto text-white">Content</a><br/>
                          <a href="#bloktoevoegen" class="scrollto text-white">Blokken toevoegen</a><br/>
                          <a href="#toolbar" class="scrollto text-white">Herpositioneer blokken</a><br/>
                          <a href="#shift" class="scrollto text-white">Shift Enter</a><br/>
                          <a href="#weergave" class="scrollto text-white">Weergave</a><br/>
                          <a href="#categorieen" class="scrollto text-white">Informatie</a><br/>
                          

                       </div>
                    </div>
               </div>
            </div>
          </div>
  <section class="intro">
    <div id="accordionExample">
      <i class="fas fa-info-circle mb-4 greentext fa-2x collapsed ml-2 ml-xl-5" data-toggle="collapse" href="#collapseExample2" data-target="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2" style="cursor:pointer"></i>
      <div class="carret collapse fade" id="collapseExample2" aria-labelledby="headingOne" data-parent="#accordionExample"></div>
    </div>
    <div class="collapse w-100 bg-graylight mb-5 border-top border-bottom" id="collapseExample2" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="container">
        <div class="row">
          <div class="p-4 p-xl-5">
               <h2>Wat is Gutenberg?</h2>
               <p class="lead">De nieuwe editor van WordPress is vernoemd naar Johannes Gutenberg. Deze Duitse uitvinder heeft een enorme impact gehad op de manier waarop boeken – en ander drukwerk – rond 1400 sneller en goedkoper gemaakt konden worden.</p>
               <p class="lead">Voor de uitvinding van Gutenberg was het namelijk zo dat pagina’s als complete stempel werden gedrukt. Wanneer er een spelfout in een bladzijde bleek te zitten moest er dus een compleet nieuwe stempel gemaakt worden om vervolgens complete pagina’s opnieuw te drukken.</p>
               <p class="lead">De uitvinding van Johannes Gutenberg maakte het mogelijk om stempels te maken van enkele karakters. Op die manier hoefde je van elk karakter maar één keer een stempel te maken en was het veel gemakkelijker om eventuele fouten te corrigeren.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-md-7 mx-auto content">
            
            <h1 class="mb-3">Met Wordpress werken</h1>
            <p>In deze handleiding zullen we je laten zien hoe Gutenberg werkt en wat je er allemaal mee kunt.</p>
            <h3>Snap hoe Gutenberg werkt</h3>
            <p>Met Gutenberg is de editor omgebouwd tot een blokkensysteem. Hierdoor heb je meer flexibiliteit bij het vullen van pagina’s. Standaard wordt de editor met meerdere blokken geleverd die ingedeeld zijn in categorieën. Wij hebben deze uitgebreid met een aantal extra features (CoBlocks).
            Hoewel de Gutenberg editor meer flexibiliteit toevoegt, betekent dit niet dat de gehele website nu met blokken in te delen is. De blokken worden geplaatst op de plek waar de content in het thema wordt opgehaald. </p>

            <h3 id="titel" class="pt-5">De titel</h3>
            <p>Dit spreekt voor zich: hier vul je de titel in voor je pagina / bericht / school. (dit is niet veranderd ten opzichte van de oude situatie)</p>


            <h3 id="content" class="pt-5">Content</h3>
            <p>Onder de titel vind je het “Content” gedeelte. Hier kun je met de Gutenberg Editor blokken plaatsen, zoals een "tekstblok", "afbeelding", "gallerij", "accordion" of "tabel". Je kunt hier gewoon beginnen met typen, zoals je dat gewend was. Wanneer je op “Enter” klikt wordt er automatisch een nieuw blok gemaakt waarin de volgende paragraph (alinea) kan worden geschreven.</p>


            <h3 id="bloktoevoegen" class="pt-5">Blokken toevoegen</h3>
             <p>Links bovenin de Gutenberg editor vind je een knop om een nieuw blok aan je pagina of bericht toe te voegen. (Door op een + icoon te klikken komt er een menu tevoorschijn.)</p>
<!--             ticket 004-->
              <img src="assets/img/blocks.jpg" alt="blok toevoegen"  class="img-fluid my-2" /><br/>
             <p>Ook als je met je muis over een bestaand blok gaat komt er een plus icoon. Het blok komt dan direct onder het bestaande blok.<img src="assets/img/blok-toevoegen.jpg" alt="blok toevoegen"  class="img-fluid text-center" /></p>

            <h3 id="toolbar" class="pt-5">Herpositioneer blokken </h3>
            <p>Ben je niet geheel tevreden over de uitstraling van je pagina? Probeer dan wat te schuiven met blokken. Met de Gutenberg editor is het erg makkelijk om blokken heen en weer te schuiven. Je kunt dit doen met de pijltjes aan de linkerkant van een blok. Het is ook mogelijk om een blok te slepen door de zes puntjes bij de pijltjes beet te pakken en het blok vervolgens te verslepen. Past een blok helemaal niet in de pagina, dan kun je het natuurlijk ook weer verwijderen. Het verwijderen van een blok kan via de extra optie. Deze vind je door op de drie puntjes in de balk boven een blok te klikken.</p>
            <img src="assets/img/nav-block.jpg" alt="switch van visueel naar code editor"  class="img-fluid my-2" /><br/><br/>
            <p>De Toolbar bestaat uit de volgende items:</p>
             <ul>
              <li>Links uitlijnen, Centreren, Rechts uitlijnen</li>
              <li>Tekst vet drukken</li>
              <li>Tekst cursiveren</li>
              <li>Link toevoegen</li>
              <li>Tekst doorhalen, Kapitalen, tekstkleur</li>
            </ul>

            <h3 id="instellingen" class="pt-5">Instellingen</h3>
            <p> Deze balk heeft twee tabbladen, Document en Blok. Logischerwijs kun je op het tabblad Document instellingen met betrekking tot het gehele document wijzigen en op het tabblad Blok enkel met betrekking tot het geselecteerde blok.</p>

            <p><img src="assets/img/sidebar.jpg" alt="sidebar"  class="img-fluid float-right pr-2 pl-3" /><br/><br/><strong>Bijvoorbeeld:</strong> Als je een blok selecteert krijg je in de sidebar (rechts) extra opties. Bijvoorbeeld bij een tabel kan je een vaste breedte selecteren of per rij een andere kleur zodat de data overzichtelijk is</p>

            <h4 class="pt-2">Een blok verwijderen</h4>
            <p>Je kunt een blok dan heel eenvoudig verwijderen door het block te selecteren en dan op backspace te klikken. Of op het “More options”-icoontje te klikken (rechts naast het blok) en vervolgens op “verwijder blok” te klikken.</p>

            <br/><br/>
            <h3 id="shift" class="pt-5">Shift Enter</h3>
            <p>Het is altijd al zo geweest dat je met een ENTER een nieuwe paragraaf start. In de Gutenberg editor wordt zo’n nieuwe paragraaf gelijk ook als een nieuw blok gezien. Wil je geen nieuw blok aanmaken maar wel op de volgende regel beginnen, gebruik dan SHIFT + ENTER.</p>

            
            
            <h3 id="weergave" class="pt-5">Weergave</h3>
            <p>Pas de weergave van Gutenberg naar wens aan Gutenberg heeft momenteel 3 opties om de editor anders weer te geven. Hiermee kan je de editor aanpassen naar wens. De 3 opties:</p>
              <ul>
                <li>Bovenste toolbar: maak de balk die normaal gesproken zichtbaar wordt als een blok geselecteerd is, altijd zichtbaar bovenaan de pagina in plaats van bij het blok zelf.</li>
                <li>Spotlight-modus: focus op het blok waar je mee bezig bent. De tekst is duidelijker zichtbaar dan de andere blokken.</li>
                <li>Schermvullende weergave: zorg voor zo min mogelijk afleiding tijdens het schrijven door de schermvullende weergave te gebruiken.</li>
              </ul>



            <h3 id="categorieen" class="pt-5">Informatie (statistieken)</h3>
            <p>Zit ik al op de 300 woorden? Kloppen mijn titels wel? De Gutenberg editor geeft deze informatie en meer weer onder de knop ‘Inhoudstructuur’ (het i-icoontje in de balk, zie afbeelding hieronder).</p>
            <p><img src="assets/img/infobutton.jpg" alt="button"  class="img-fluid my-2" /></p>
            <img src="assets/img/info.jpg" alt="krijg je meer informatie over je pagina of bericht"  class="img-fluid my-2" /><br/>        
           </div>
        </div>
      </div>
  </section>
   
  <?php include "templates/footer.php"; ?>
  </body>
</html>