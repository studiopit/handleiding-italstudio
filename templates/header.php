<!-- header-->
<header class="header w-100 text-xl-center">	  
	
	<nav class="navbar navbar-expand-sm">
    <div class="logoital">
      <a href="./"><?php echo file_get_contents( 'assets/img/logo-italstudio.svg' ); ?></a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      <span class="navbar-toggler-icon"></span>
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav ml-4">
	      <li class="nav-item py-2 mr-3"><a class="nav-link" href="index.php">Home</a></li>
	      <li class="nav-item py-2"><a class="nav-link" href="themesettings.php">Theme settings</a></li>
        <li class="nav-item py-2"><a class="nav-link" href="scholen.php">Scholen</a></li>
        <li class="nav-item py-2"><a class="nav-link" href="cursussen.php">Cursussen</a></li>
	    </ul>
    </div>
  </nav>
</header>
