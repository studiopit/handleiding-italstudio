<footer class="main-footer bg-blue">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-lg-6 mb-3 mb-lg-0 text-white">
        <h5 class="footer-heading text-white mb-0">Adres</h5>
<!--          Ticket 001-->
        <p>Dommelstraat 36 <br/>
        4105 ZC Culemborg<br/>
        info@italstudio.nl<br/>
        T: (+31) 0345 52 01 52<br/>
        K.v.K. 1103 3273</p>
      </div>

      <div class="col-sm-6 col-lg-6 mb-3 text-white">
        <h5 class="footer-heading text-white mb-0">Over ons</h5>
        <p>Italstudio is al 30 jaar Italië-specialist en de meest toonaangevende organisatie voor jouw cursus Italiaans en studiereizen naar Italië. Je vindt bij ons Italiaanse taalcursussen voor alle leeftijden en zo’n 45 bestemmingen in heel Italië. Of het nu om een vakantiecursus Italiaans, examencursus, tussenjaar of zakencursus gaat, alles is mogelijk en het hele jaar door!</p>
      </div>
  </div>
</footer>

<!-- Copyright -->
<!--Ticket 003-->
<div class="copyrights bg-white text-dark py-3">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <p class="copyrights-text">&copy; <?php echo date('Y'); ?> Italstudio</p>
        </div>
       </div>
    </div>
</div>

<!-- JavaScript files -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="vendor/easing/easing.min.js"></script>
<script src="vendor/lightbox2/js/lightbox.js"></script>
<script src="assets/js/front.js"></script>